import { init, RematchDispatch, RematchRootState, RematchStore } from '@rematch/core';
import { models, RootModel } from './models';
import { useMemo } from 'react';
import { useDispatch } from 'react-redux';

export const initStore = (initialState = {}) =>
    init({
        models: models,
        redux: {
            initialState,
        },
    });

export const initializeStore = (preloadedState) => {
    let store: RematchStore<RootModel, Record<string, never>> | undefined;
    let _store: RematchStore<RootModel, Record<string, never>> = store ?? initStore(preloadedState);

    if (preloadedState && store) {
        _store = initStore({
            ...store.getState(),
            ...preloadedState,
        });
        store = undefined;
    }
    if (typeof window === 'undefined') return _store;
    if (!store) store = _store;

    return _store;
};

export const store = initializeStore({});
export const dispatch = store.dispatch;
export type Dispatch = RematchDispatch<RootModel>;
export type RootState = RematchRootState<RootModel>;
export const checkServer = () => {
    return typeof window === 'undefined';
};

export const useRematchDispatch = (selector) => {
    const dispatch = useDispatch();
    return selector(dispatch);
};
