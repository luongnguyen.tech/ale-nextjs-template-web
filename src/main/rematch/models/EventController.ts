import { createModel } from '@rematch/core';
import { RootModel } from '.';

type StateEventController = {};

export const eventController = createModel<RootModel>()({
    state: {} as StateEventController,
    reducers: {
        update(state, payload: StateEventController) {
            return { ...state, ...payload };
        },
    },
    effects: (dispatch) => ({
        setIsLoading(isLoading: boolean) {
            dispatch.eventController.update(isLoading);
        },
    }),
});
