import { Models } from '@rematch/core';
import { eventController } from './EventController';
export interface RootModel extends Models<RootModel> {
    eventController: typeof eventController;
}
export const models: RootModel = { eventController };
