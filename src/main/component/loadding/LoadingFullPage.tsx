export const LoadingFullPage = () => {
    return (
        <div
            style={{
                position: 'fixed',
                top: 0,
                left: 0,
                background: '#18315B',
                color: 'white',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                width: '100%',
                height: '100vh',
                zIndex: 10000,
            }}
        >
            <h2>BeeMall</h2>
            <div
                style={{
                    width: 300,
                    borderRadius: 20,
                    border: `2px solid #E81B63`,
                    height: 30,
                    overflow: 'hidden',
                    marginTop: 30,
                }}
            >
                <div
                    style={{
                        width: '50%',
                        height: '100%',
                        background: '#E81B63',
                    }}
                ></div>
            </div>
        </div>
    );
};
