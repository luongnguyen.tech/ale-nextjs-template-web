import GuestPage from '@BaseWeb/component/type-page/GuestPage';
import { LayoutProps } from '@BaseWeb/types/CommonNextJs';
import React from 'react';
import { LoadingFullPage } from '../loadding/LoadingFullPage';
import AuthenLayout from './AuthenLayout';
import PublicLayout from './PublicLayout';

type TypeLayoutCustom = (props: LayoutProps) => JSX.Element;
type Props = {} & LayoutProps;
/**
 * @author: luong
 * @description: Layout for public and private layout. If have user, the private layout will be displayed, and if not, the public layout will be displayed
 */
const MainLayout = (props: Props) => {
    return (
        <GuestPage layoutPublic={PublicLayout} layoutPrivate={AuthenLayout} layoutLoading={LoadingFullPage()}>
            {props.children}
        </GuestPage>
    );
};

export default MainLayout;
