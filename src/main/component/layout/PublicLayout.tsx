import { DefaultWrapper } from '@BaseWeb/component/wrapper/DefaultWrapper';
import { LayoutProps } from '@BaseWeb/types/CommonNextJs';
import { Grid, Typography } from '@mui/material';
import React from 'react';
import MainLayout from './MainLayout';

type Props = {} & LayoutProps;
const PublicLayout = (props: Props) => {
    return (
        <Grid container justifyContent="center">
            <Grid item xs={12} md={8}>
                <DefaultWrapper useDefaultMenu={true}>{props.children}</DefaultWrapper>
            </Grid>
        </Grid>
    );
};
export default PublicLayout;
