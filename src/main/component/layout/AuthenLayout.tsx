import PrivatePage from '@BaseWeb/component/type-page/PrivatePage';
import { useAuthen } from '@BaseWeb/hook/useAuthen';
import { LayoutProps } from '@BaseWeb/types/CommonNextJs';
import { Grid, Typography } from '@mui/material';
import React, { useEffect } from 'react';

type Props = {
    layoutLoading?: JSX.Element;
} & LayoutProps;
const AuthenLayout = (props: Props) => {
    return <PrivatePage>{props.children}</PrivatePage>;
};
export default AuthenLayout;
