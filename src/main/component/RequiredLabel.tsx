import React from 'react';

export const RequiredLabel = ({ label }: { label: string }) => {
    return (
        <>
            {label}
            <span style={{ color: 'red' }}>{' *'}</span>
        </>
    );
};
