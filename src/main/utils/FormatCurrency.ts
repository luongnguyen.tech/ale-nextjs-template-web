export const formatCurrency = (number) =>
    new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 8 }).format(number);
