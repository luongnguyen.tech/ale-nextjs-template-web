export function toParamsString(params: { key?: string; value?: string }[]) {
    return params
        .filter((p) => p.key && p.key.length > 0 && p.value && p.value.length > 0)
        .map((p) => `${p.key}=${p.value}`)
        .join('&');
}
