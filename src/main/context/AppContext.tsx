import { AppNotification } from '@BaseWeb/component/AppNotification';
import { AuthenContext, useAuthenContext } from '@BaseWeb/hook/useAuthen';
import useGoogleAnalyticContext, { GoogleAnalyticContext } from '@BaseWeb/hook/useGoogleAnalytic';
import useLoadingContext, { LoadingContext } from '@BaseWeb/hook/useLoading';
import { MobileContext, useMobileContext } from '@BaseWeb/hook/useMobile';
import useNotificationContext, { NotificationContext } from '@BaseWeb/hook/useNotification';
import { MyAppProps } from 'src/pages/_app';

export type TypeAppContext = {
    loading: ReturnType<typeof useLoadingContext>;
    mobile: ReturnType<typeof useMobileContext>;
    notification: ReturnType<typeof useNotificationContext>;
    authen: ReturnType<typeof useAuthenContext>;
    googleAnalytics: ReturnType<typeof useGoogleAnalyticContext>;
};

const main: TypeAppContext = {} as TypeAppContext;
export function AppContext({ children }: { children: any } & MyAppProps) {
    const mobile = useMobileContext(main.loading);
    const loading = useLoadingContext();
    const notification = useNotificationContext();
    const authen = useAuthenContext();
    const googleAnalytics = useGoogleAnalyticContext();
    main.mobile = mobile;
    main.loading = loading;
    main.notification = notification;
    main.authen = authen;
    main.googleAnalytics = googleAnalytics;

    return (
        <MobileContext.Provider value={mobile}>
            <LoadingContext.Provider value={loading}>
                <NotificationContext.Provider value={notification}>
                    <AuthenContext.Provider value={authen}>
                        <GoogleAnalyticContext.Provider value={googleAnalytics}>
                            {children}
                            <AppNotification />
                        </GoogleAnalyticContext.Provider>
                    </AuthenContext.Provider>
                </NotificationContext.Provider>
            </LoadingContext.Provider>
        </MobileContext.Provider>
    );
}
