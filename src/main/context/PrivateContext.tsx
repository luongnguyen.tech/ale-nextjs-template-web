import { LayoutProps } from '@BaseWeb/types/CommonNextJs';

export type PrivateContextType = {};
export function PrivateContext(props: LayoutProps) {
    const main: Partial<PrivateContextType> = {};
    return <>{props.children}</>;
}
