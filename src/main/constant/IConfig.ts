import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
export interface IConfig {
    apiGateway: {
        account: string;
        ecommerce: string;
    };
}

export const localConfig: IConfig = {
    apiGateway: {
        // account: process.env.REACT_APP_GATEWAY_ACCOUNT || 'http://localhost:6003',
        account: process.env.REACT_APP_GATEWAY_ACCOUNT || 'http://192.168.1.252:6003',
        ecommerce: process.env.REACT_APP_GATEWAY_ECOMMERCE || 'http://192.168.1.252:6003',
    },
};

export const prodConfig: IConfig = {
    apiGateway: {
        account: 'http://localhost:6003',
        ecommerce: 'http://localhost:6003',
    },
};
console.log({ env: process.env.REACT_APP_ENV });

export const appConfig =
    process.env.REACT_APP_ENV === 'production'
        ? prodConfig
        : process.env.REACT_APP_ENV === 'staging'
        ? localConfig
        : process.env.REACT_APP_ENV === 'development'
        ? localConfig
        : localConfig;

const firebaseConfig = {
    apiKey: 'AIzaSyBVbYINTtf0pz-AX2zJjnZYtckgCVcfwWc',
    authDomain: 'bee-gift.firebaseapp.com',
    projectId: 'bee-gift',
    storageBucket: 'bee-gift.appspot.com',
    messagingSenderId: '994790539417',
    appId: '1:994790539417:web:086f908a1e8e9835b43808',
    measurementId: 'G-8Y31DVKBRK',
};

const app = initializeApp(firebaseConfig);
export const authentication = getAuth(app);
