import { axiosCreator } from '@BaseWeb/controller/AxiosCreator';
import { appConfig } from '@Config/IConfig';
import { BaseUserController } from '@CoreAccount/controller/http/BaseUserController';

export const axiosClient = axiosCreator({
    url: appConfig.apiGateway.account,
});
console.log({ appConfig });

export const userController = new BaseUserController(appConfig.apiGateway.ecommerce,"user",axiosClient); // : change to user Controller

