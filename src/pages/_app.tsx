import { ThemeConfigNextJsProvider } from '@BaseWeb/theme';
import createEmotionCache from '@Config/theme/createEmotionCache';
import { CacheProvider, EmotionCache } from '@emotion/react';
import { createTheme, CssBaseline, ThemeProvider } from '@mui/material';
import { AppPropsWithLayout } from 'next';
import Head from 'next/head';
import React from 'react';
import { Provider } from 'react-redux';
import MainLayout from 'src/main/component/layout/MainLayout';
import { AppContext } from 'src/main/context/AppContext';
import { store } from 'src/main/rematch/store';

export interface MyAppProps extends AppPropsWithLayout {
    emotionCache?: EmotionCache;
}
const clientSideEmotionCache = createEmotionCache();

const theme2 = createTheme({});

function MyApp(props: MyAppProps) {
    const { Component, pageProps, emotionCache = clientSideEmotionCache } = props;
    const Layout = Component.Layout ?? MainLayout;

    return (
        <Provider store={store as any}>
            <CacheProvider value={emotionCache}>
                <Head>
                    <meta name="viewport" content="initial-scale=1, width=device-width" />
                </Head>
                <ThemeConfigNextJsProvider>
                    <AppContext {...props}>
                        <Layout>
                            <Component {...pageProps} />
                        </Layout>
                    </AppContext>
                </ThemeConfigNextJsProvider>
            </CacheProvider>
        </Provider>
    );
}
// MyApp.getInitialProps = async (appContext) => {
//   const appProps = await App.getInitialProps(appContext)
//   return { ...appProps }
// }
export default MyApp;
