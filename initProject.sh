
echo  $1

rm -rf src/ale-base-web src/main/submodule/ale-base-model src/main/submodule/ale-account-model .gitmodules

rm -rf .git

git init --initial-branch=main

git remote add origin $1

git add .

git commit -m "Initial commit"

git push -u origin main

git submodule add git@gitlab.com:ale-tech/share/ale-base-web.git src/ale-base-web

git submodule add git@gitlab.com:ale-tech/share/ale-base-model.git src/main/submodule/ale-base-model

git submodule add git@gitlab.com:ale-tech/share/ale-account-model.git src/main/submodule/ale-account-model

git add .

git commit -m "update model"

git push